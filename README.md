# mididings-scripts

For now, I have just the one script.

## controller-modifier.py

A [Mididings](http://das.nasophon.de/mididings/) script that:

  1. enables the use of midi controls as modifier keys to alter the
     behaviour of other controls (like shift/ctrl/alt etc on a computer
     keyboard);
  2. enables distinguishing between a long press and a short 'click' of
     a control to modify behaviour;
  3. has the potential to massively increase the capabilities of your
     midi controller when using it with a DAW or in live performance.

### How to use

Configure the `USABLE_CONTROLS` constant to your liking.  You need to know
what MIDI data your controller is sending when you press any of the controls
you're interested in.  The usual way is to use a MIDI monitoring program or
plugin.

Its structure consists of arbitrary names for the buttons on your controller
(whatever you find meaningful), each having one or more *control profiles* which
reflect what the controller actually sends (i.e. button 'on' and 'off' events),
either as a two-press toggle or a press-and-release trigger pair (depending on
how your controller hardware is configured).  This script attempts to match the
incoming MIDI event with a *control profile* to internally identify the control.

In addition to the *control profiles* there are *modifier profiles* that
define what alternative data you want sent when a control is pressed with a
modifier button also pressed.

For instance, if you want the PLAY transport button to send something
different when the LOOP button is pressed at the same time, you would create
a set of profiles as follows:

     'loop': {
       'on':       {...<data the controller sends>...},     <-- control profile
       'off':      {...<data the controller sends>...}
     },
     'play': {
       'on':       {...<data the controller sends>...},
       'off':      {...<data the controller sends>...},
       'loop_on':  {...<data you want sent instead>...},    <-- modifier profile
       'loop_off': {...<data you want sent instead>...}
     }, etc...

Ensure the prefix of the profile name (i.e. `'loop_'`) matches the name of the
control you want to use as the modifier and that you have configured that
control elsewhere in the structure.  Also, ensure you use lowercase in your
identifiers.  The *control profiles* identifier keys must be `'on'`, `'off'` or
`'click'`.  The *modifier profile* identifier keys must be suffixed with `'_on'`,
`'_off'` or `'_click'`.

Each profile can have any/all of the following entries:

  - channel
  - data1
  - data2

For example:

    'loop_on': {'channel': 16, 'data1': 100, 'data2': 127}

For the *control profiles*, you can decide which of these elements you wish to
match and omit any others.  For example, if you want the matching to be
channel-agnostic, omit the `'channel'` element.

For the *modifier profiles* you likewise can decide which of these elements you
want to change in the output and omit the others (which will be passed unchanged
from the incoming MIDI event).

Once configured, run this script (i.e. `python controller-modifier.py`) and make
the software MIDI connections thus:

    YOUR CONTROLLER --> THIS SCRIPT --> YOUR DAW, SOFTSYNTH etc

The rest is up to you, such as ensuring your DAW etc knows how to handle your
modified MIDI events that this script will pass on.

Use argument `-v` at the command line to see how MIDI events are being processed.
Omitting this argument makes the script run silently (unless there is an
unforeseen error).

The possibilities are limited only by your imagination.

### Inspiration

This script came into being after reading about [DirectLink](https://m-audio.com/kb/article/1686) in the Owner's
Manual for the M-Audio Oxygen49 v3 controller, where a small handful of
proprietary DAWs support enhanced functionality of the controller, thus:

  1. LOOP button quick press:
      - toggle between 'Instrument' or 'Mixer' modes
          - Mixer mode: faders and knobs work as channel gain and pan controls
          - Instrument mode: faders and knobs work as instrument controls for a
        selected track's instrument plugin
  2. Without the LOOP button pressed:
      - transport controls work 'as expected'
  3. While LOOP button is pressed:
      - LOOP + REW  = goto start
      - LOOP + FWD  = goto end
      - LOOP + STOP = undo
      - LOOP + PLAY = loop play mode
      - LOOP + REC  = loop record mode
  4. Without the master slider button pressed:
      - slider buttons act as track mute toggles
      - knobs act as track left pan controls
  5. While the master slider button is pressed:
      - slider buttons act as track solo toggles
      - knobs act as track right pan controls

DirectLink works with ProTools and a few other proprietary DAWS, but not for
[Ardour](https://ardour.org/) and many others.  Ardour's midi mapping only detects single events (in
other words, it can't detect if you've pressed two buttons at once, for
example), so I set to thinking about how I could hack my own version of
this type of functionality.

This is where this script comes in.
With the right configuration, much of the above and much more is now possible, for
sending modified control events to absolutely any midi-aware software or
hardware.  How cool is that?  ;)
