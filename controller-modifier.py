#!/usr/bin/python
"""
controller-modifier.py

A Mididings script that enables the use of midi controls as modifier keys to
alter the behaviour of other controls (like shift/ctrl/alt etc on a computer
keyboard).

For more information: https://gitlab.com/mark_whibley/mididings-scripts
"""

import sys, time
from mididings import *
from mididings.extra.inotify import AutoRestart

config(
  backend='jack',
  client_name='controller-modifier',
  in_ports=['in'],
  out_ports=['out']
)

hook(
  AutoRestart()
)

# Constants

CLICK_DURATION = 0.2 # seconds; on and off events are timed between each other.
                     # If both events occur within this time frame, the control
                     # is interpreted as a 'click' which can be distinct from
                     # press and release events.

# My controller is an M-Audio Oxygen49 mk3
USABLE_CONTROLS = {
  'track_prev': {
    'on':             {'channel': 16, 'data1': 110, 'data2': 127}, # Ardour: select next track
    'off':            {'channel': 16, 'data1': 110, 'data2': 0},
    'master_btn_on':  {'channel': 16, 'data1': 110, 'data2': 40}   # Ardour: increment bank number
  },
  'track_next': {
    'on':             {'channel': 16, 'data1': 111, 'data2': 127}, # Ardour: select previous track
    'off':            {'channel': 16, 'data1': 111, 'data2': 0},
    'master_btn_on':  {'channel': 16, 'data1': 111, 'data2': 40}   # Ardour: decrement bank number
  },
  'fdr_btn_1': {
    'on':             {'channel': 16, 'data1': 49, 'data2': 127},  # Ardour: mute track B1
    'off':            {'channel': 16, 'data1': 49, 'data2': 0},
    'master_btn_on':  {'channel': 16, 'data1': 65, 'data2': 127},  # Ardour: solo track B1
    'master_btn_off': {'channel': 16, 'data1': 65, 'data2': 0},
    'rec_on':         {'channel': 16, 'data1': 73, 'data2': 127},  # Ardour: record arm track B1
    'rec_off':        {'channel': 16, 'data1': 73, 'data2': 0}
  },
  'fdr_btn_2': {
    'on':             {'channel': 16, 'data1': 50, 'data2': 127},  # ditto, B2
    'off':            {'channel': 16, 'data1': 50, 'data2': 0},
    'master_btn_on':  {'channel': 16, 'data1': 66, 'data2': 127},
    'master_btn_off': {'channel': 16, 'data1': 66, 'data2': 0},
    'rec_on':         {'channel': 16, 'data1': 74, 'data2': 127},
    'rec_off':        {'channel': 16, 'data1': 74, 'data2': 0}
  },
  'fdr_btn_3': {
    'on':             {'channel': 16, 'data1': 51, 'data2': 127},  # ditto, B3
    'off':            {'channel': 16, 'data1': 51, 'data2': 0},
    'master_btn_on':  {'channel': 16, 'data1': 67, 'data2': 127},
    'master_btn_off': {'channel': 16, 'data1': 67, 'data2': 0},
    'rec_on':         {'channel': 16, 'data1': 75, 'data2': 127},
    'rec_off':        {'channel': 16, 'data1': 75, 'data2': 0}
},
  'fdr_btn_4': {
    'on':             {'channel': 16, 'data1': 52, 'data2': 127},  # ditto, B4
    'off':            {'channel': 16, 'data1': 52, 'data2': 0},
    'master_btn_on':  {'channel': 16, 'data1': 68, 'data2': 127},
    'master_btn_off': {'channel': 16, 'data1': 68, 'data2': 0},
    'rec_on':         {'channel': 16, 'data1': 76, 'data2': 127},
    'rec_off':        {'channel': 16, 'data1': 76, 'data2': 0}
  },
  'fdr_btn_5': {
    'on':             {'channel': 16, 'data1': 53, 'data2': 127},  # ditto, B5
    'off':            {'channel': 16, 'data1': 53, 'data2': 0},
    'master_btn_on':  {'channel': 16, 'data1': 69, 'data2': 127},
    'master_btn_off': {'channel': 16, 'data1': 69, 'data2': 0},
    'rec_on':         {'channel': 16, 'data1': 77, 'data2': 127},
    'rec_off':        {'channel': 16, 'data1': 77, 'data2': 0}
  },
  'fdr_btn_6': {
    'on':             {'channel': 16, 'data1': 54, 'data2': 127},  # ditto, B6
    'off':            {'channel': 16, 'data1': 54, 'data2': 0},
    'master_btn_on':  {'channel': 16, 'data1': 70, 'data2': 127},
    'master_btn_off': {'channel': 16, 'data1': 70, 'data2': 0},
    'rec_on':         {'channel': 16, 'data1': 78, 'data2': 127},
    'rec_off':        {'channel': 16, 'data1': 78, 'data2': 0}
  },
  'fdr_btn_7': {
    'on':             {'channel': 16, 'data1': 55, 'data2': 127},  # ditto, B7
    'off':            {'channel': 16, 'data1': 55, 'data2': 0},
    'master_btn_on':  {'channel': 16, 'data1': 71, 'data2': 127},
    'master_btn_off': {'channel': 16, 'data1': 71, 'data2': 0},
    'rec_on':         {'channel': 16, 'data1': 79, 'data2': 127},
    'rec_off':        {'channel': 16, 'data1': 79, 'data2': 0}
  },
  'fdr_btn_8': {
    'on':             {'channel': 16, 'data1': 56, 'data2': 127},  # ditto, B8
    'off':            {'channel': 16, 'data1': 56, 'data2': 0},
    'master_btn_on':  {'channel': 16, 'data1': 72, 'data2': 127},
    'master_btn_off': {'channel': 16, 'data1': 72, 'data2': 0},
    'rec_on':         {'channel': 16, 'data1': 80, 'data2': 127},
    'rec_off':        {'channel': 16, 'data1': 80, 'data2': 0}
  },
  'master_btn': {
    'on':  {'channel': 16, 'data1': 57, 'data2': 127},             # Unused except as a modifier button
    'off': {'channel': 16, 'data1': 57, 'data2': 0}
  },
  'loop': {
    'on':    {'channel': 16, 'data1': 113, 'data2': 127},          # Unused except as a modifier button
    'off':   {'channel': 16, 'data1': 113, 'data2': 0},
    'click': {'channel': 16, 'data1': 103, 'data2': 127}           # Ardour: loop toggle
  },
  'rew': {
    'on':       {'channel': 16, 'data1': 114, 'data2': 127},       # Ardour: rewind (subsequent presses increase rewind speed)
    'off':      {'channel': 16, 'data1': 114, 'data2': 0},
    'loop_on':  {'channel': 16, 'data1': 104, 'data2': 127},       # Ardour: goto start
    'loop_off': {'channel': 16, 'data1': 104, 'data2': 0}
  },
  'fwd': {
    'on':       {'channel': 16, 'data1': 115, 'data2': 127},       # Ardour: forward (subsequent presses increase forward speed)
    'off':      {'channel': 16, 'data1': 115, 'data2': 0},
    'loop_on':  {'channel': 16, 'data1': 105, 'data2': 127},       # Ardour: goto end
    'loop_off': {'channel': 16, 'data1': 105, 'data2': 0}
  },
  'stop': {
    'on':       {'channel': 16, 'data1': 116, 'data2': 127},       # Ardour: stop
    'off':      {'channel': 16, 'data1': 116, 'data2': 0},
    'loop_on':  {'channel': 16, 'data1': 106, 'data2': 127},       # Ardour: stop and forget last take
    'loop_off': {'channel': 16, 'data1': 106, 'data2': 0}
  },
  'play': {
    'on':       {'channel': 16, 'data1': 117, 'data2': 127},       # Ardour: play/continue/stop
    'off':      {'channel': 16, 'data1': 117, 'data2': 0},
    'loop_on':  {'channel': 16, 'data1': 107, 'data2': 127},       # Ardour: loop play
    'loop_off': {'channel': 16, 'data1': 107, 'data2': 0}
  },
  'rec': {
    'on':       {'channel': 16, 'data1': 118, 'data2': 127},       # Unused except as a modifier
    'off':      {'channel': 16, 'data1': 118, 'data2': 0},
    'click':    {'channel': 16, 'data1': 118, 'data2': 64},        # Ardour: master record arm
    'loop_on':  {'channel': 16, 'data1': 108, 'data2': 127},       # Ardour: can't remember!
    'loop_off': {'channel': 16, 'data1': 108, 'data2': 0}
  }
}


# Variables for tracking state between events

controls_pressed = ['none', 'none']
press_count = 0
press_timers = {
  'first': {'on': 0, 'off': 0, 'duration': 0},
  'second': {'on': 0, 'off': 0, 'duration': 0}
}


def check_modifiers(ev):
  global controls_pressed
  global press_count
  global press_timers
  control = 0
  modifier_profile = 0

  # Print verbose event data to console if '-v' argument supplied
  verbose = (len(sys.argv) == 2 and sys.argv[1] == '-v')

  if verbose: print ('\n>>> MIDI IN: channel:%d, data1:%d, data2:%d') \
    % (ev.channel, ev.data1, ev.data2)

  # Search for a control profile matching the incoming MIDI event
  for control_name, control_config in USABLE_CONTROLS.items():
    for toggle, profile in control_config.items():
      match_test = {
        'event': {'channel': ev.channel, 'data1': ev.data1, 'data2': ev.data2},
        'profile': {}
      }

      if profile.has_key('channel'):
        match_test['profile']['channel'] = profile['channel']
      else:
        match_test['event'].pop('channel')

      if profile.has_key('data1'):
        match_test['profile']['data1'] = profile['data1']
      else:
        match_test['event'].pop('data1')

      if profile.has_key('data2'):
        match_test['profile']['data2'] = profile['data2']
      else:
        match_test['event'].pop('data2')

      if cmp(match_test['event'], match_test['profile']) == 0 \
        and toggle in ['on', 'off']: # We have a match
        control = {
          'name': control_name, 
          'toggle': toggle, 
          'profiles': USABLE_CONTROLS[control_name]
        }

      if control: break # no further 'for' loop iterations needed
    if control: break # no further 'for' loop iterations needed 

  if not control:
    if verbose: print ('<<< MIDI THRU: no control profile match')
    return ev

  if verbose: print ('    control profile matched for "%s"') % (control['name'])

  if control['toggle'] == 'on': # A control has been pressed
    press_count += 1
    if press_count == 1:
      # This is the only control pressed for now - it could be used as a
      # modifier for the next controller event if it remains pressed
      press_timers['first']['on'] = time.time()
      controls_pressed[0] = control['name']
      if verbose: print ('<<< MIDI THRU: "%s" pressed (possible modifier)') \
        % (control['name'])
      return ev

    elif press_count == 2:
      # This is the second control to be simultaneously pressed
      press_timers['second']['on'] = time.time()
      controls_pressed[1] = control['name']
      modifier_profile = controls_pressed[0] + '_on'

    else: # press_count > 2
      # Further simultaneous presses (i.e. > 2) are rejected
      if verbose: print ('|<< MIDI BLOCKED: more than 2 simultaneous control presses')
      return None

  else: # if control['toggle'] == 'off': # A control has been released
    press_count -= 1
    if press_count > 1:
      # This is a third (or greater) control that's been released - MIDI rejected
      if verbose: print ('|<< MIDI BLOCKED: surplus control (i.e. third or greater) released')
      return None

    elif press_count == 1:
      # This is a second control that's been released
      if control['name'] == controls_pressed[0]:
        # The first control has been prematurely released before the second
        # Make the remaining pressed control the new potential modifier
        controls_pressed[0] = controls_pressed[1]
        controls_pressed[1] = 'none'
        if verbose: print ('|<< MIDI BLOCKED: "%s" not released in the expected reverse order') \
          % (control['name'])
        return None

      else:
        # The second control was correctly released before the first
        press_timers['second']['off'] = time.time()
        press_timers['second']['duration'] = press_timers['second']['off'] - press_timers['second']['on']
        if verbose: print ('    "%s" was pressed for %2.2fs') \
          % (control['name'], press_timers['second']['duration'])
        if press_timers['second']['duration'] <= CLICK_DURATION:
          modifier_profile = controls_pressed[0] + '_click'
        else:
          modifier_profile = controls_pressed[0] + '_off'

        controls_pressed[1] = 'none'

    elif press_count == 0:
      # The only pressed control has been released
      press_timers['first']['off'] = time.time()
      press_timers['first']['duration'] = press_timers['first']['off'] - press_timers['first']['on']
      if verbose: print ('    "%s" was pressed for %2.2fs') \
        % (control['name'], press_timers['first']['duration'])
      controls_pressed[0] = 'none'

      if press_timers['first']['duration'] <= CLICK_DURATION:
        modifier_profile = 'click'
      else:
        modifier_profile = 0
        if verbose: print ('<<< MIDI THRU: "%s" released') % (control['name'])
        return ev

    else: # press_count is negative???  Surely not!
      # This shouldn't happen, but catch it just in case
      print ('<<< MIDI THRU: ERROR - press_count:%d in an inconsistent state') % (press_count)
      return ev

  if modifier_profile:
    # Check if a modifier profile exists for this combination of pressed controls
    if not control['profiles'].has_key(modifier_profile):
      if verbose: print ('<<< MIDI THRU: modifier profile "%s" not configured for control "%s"') \
        % (modifier_profile, control['name'])

      return ev

    else:
      # Modifier profile has been found - use it
      if verbose: print ('    modifier profile "%s" found for control "%s"') \
        % (modifier_profile, control['name'])

      if control['profiles'][modifier_profile].has_key('channel'):
        ev.channel = control['profiles'][modifier_profile]['channel']

      if control['profiles'][modifier_profile].has_key('data1'):
        ev.data1 = control['profiles'][modifier_profile]['data1']

      if control['profiles'][modifier_profile].has_key('data2'):
        ev.data2 = control['profiles'][modifier_profile]['data2']

      if verbose:
        print ('<<< MIDI OUT: channel:%d, data1:%s, data2:%d') \
        % (ev.channel, ev.data1, ev.data2)

      return ev

run(
  Process(check_modifiers)
)
